package com.example.theinstuls;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void NextPage (View view) {
        Button btn_next = (Button) findViewById(R.id.button);
        Intent intent = new Intent(MainActivity.this,thesky.class);
        startActivity(intent);
    }
}